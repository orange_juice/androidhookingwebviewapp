package com.philgookang.webviewhooking.libs;

import android.app.Activity;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.TaskStackBuilder;
import android.webkit.WebResourceRequest;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toast;

import com.philgookang.webviewhooking.MainActivity;
import com.philgookang.webviewhooking.R;

import static android.content.Context.NOTIFICATION_SERVICE;

public class HookWebViewClient extends WebViewClient {

    // the parent activity
    private Activity mActivity;

    public HookWebViewClient(Activity mActivity) {
        this.mActivity = mActivity;
    }

    @Override
    public boolean shouldOverrideUrlLoading(WebView view, WebResourceRequest request) {

        // get request uri
        Uri uri = (Uri)request.getUrl();

        // get url
        String url = uri.toString();

        // check if its something we need to process
        if (url.startsWith("onoffmix:")) {

            // find action
            String action = uri.getQueryParameter("action");

            if ( action.contentEquals("toast")) {

                // call toast
                this.callToast();

                // lets handle it ourself
                return true;
            } else if ( action.contentEquals("notification")) {

                // call notification
                this.callNotification();

                // lets handle it ourself
                return true;
            }
        }

        // nope, we do not want to do anything!
        return false;
    }

    public void callToast() {
        Toast.makeText(this.mActivity, "토스트", Toast.LENGTH_SHORT).show();
    }

    public void callNotification() {

        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this.mActivity)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setContentTitle("온오프믹스")
                .setContentText("알림")
                .setContentIntent(PendingIntent.getActivity(this.mActivity, (int) System.currentTimeMillis(), new Intent(), 0));

        NotificationManager notificationManager = (NotificationManager)this.mActivity.getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(0, notificationBuilder.build());
    }
}
