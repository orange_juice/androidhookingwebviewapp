package com.philgookang.webviewhooking;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebView;
import android.widget.Toast;

import com.philgookang.webviewhooking.libs.HookWebChromeClient;
import com.philgookang.webviewhooking.libs.HookWebViewClient;

public class MainActivity extends Activity {

    // hybrid webview
    private WebView mWebView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // connect webview variable to object in view
        mWebView = (WebView) this.findViewById(R.id.mWebView);

        // webview settings to enhance ux
        mWebView.getSettings().setJavaScriptEnabled(true);
        mWebView.getSettings().setDomStorageEnabled(true);
        mWebView.setLayerType(View.LAYER_TYPE_SOFTWARE, null);

        // set normal client & chrome client
        mWebView.setWebChromeClient(new HookWebChromeClient());
        mWebView.setWebViewClient(new HookWebViewClient(this));

        // load url
        mWebView.loadUrl("http://onoffmix.philgookang.com/");
    }
}